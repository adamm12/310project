class Crime < ActiveRecord::Base
    def self.import(file)
        CSV.foreach(file.path, headers: true) do |row|
            Crime.create! (row.to_hash)
        end
    end
end
