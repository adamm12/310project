json.array!(@crimes) do |crime|
  json.extract! crime, :id, :TYPE, :HUNDRED_BLOCK
  json.url crime_url(crime, format: :json)
end
