class CreateCrimes < ActiveRecord::Migration
  def change
    create_table :crimes do |t|
      t.string :type
      t.string :location

      t.timestamps null: false
    end
  end
end
